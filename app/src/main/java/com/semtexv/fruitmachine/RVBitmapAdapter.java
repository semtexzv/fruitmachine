package com.semtexv.fruitmachine;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

class RVBitmapAdapter extends RecyclerView.Adapter<RVBitmapAdapter.SlotViewHolder> {
    List<Bitmap> images;

    RVBitmapAdapter(List<Bitmap> images) {
        this.images = images;
    }

    @Override
    public SlotViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.slot_view, viewGroup, false);


        return new SlotViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SlotViewHolder vh, int i) {
        vh.image.setImageBitmap(images.get(i % images.size()));
        vh.index = i;

    }

    @Override
    public int getItemCount() {
        if (images.size() == 0)
            return 0;
        return 8000;
    }

    public static class SlotViewHolder extends RecyclerView.ViewHolder {
        public int index;
        public ImageView image = (ImageView) itemView;

        public SlotViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView;
        }
    }
}
