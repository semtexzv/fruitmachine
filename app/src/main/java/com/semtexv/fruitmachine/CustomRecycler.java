package com.semtexv.fruitmachine;

/**
 * Created by Semtexzv on 4/13/2016.
 */

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class CustomRecycler extends RecyclerView {

    public CustomRecycler(Context context){
        super(context);
    }

    public CustomRecycler(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public CustomRecycler(Context context, AttributeSet attrs, int style){
        super(context, attrs, style);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        //Ignore scroll events.
        if (ev.getAction() == MotionEvent.ACTION_MOVE)
            return true;

        return super.dispatchTouchEvent(ev);
    }

    //Keep aspect ratio 1:3
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        if(width*3 > height) {
            setMeasuredDimension(height/3, height);
        }
        else {
            setMeasuredDimension(width, width*3);
    }   }
}
