package com.semtexv.fruitmachine;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;


public class PlayActivity extends AppCompatActivity {

    FruitMachineApplication app;

    RecyclerView left;
    RecyclerView center;
    RecyclerView right;

    Button button;

    DrawerLayout drawer;
    LinearLayout drawerContents;
    ListView historyList;

    SelectorLinearLayout centerLayout;

    final int scrollPeriod = 16;
    final int itemCount = 4;


    final Random random = new Random();
    CountDownTimer timer;

    //ArrayAdapter<Bitmap> adapter;
    ArrayAdapter<Integer> histAdapter;
    CustomLayoutManager layoutManager;

    RVBitmapAdapter adapter;


    LinearInterpolator linear;


    public class ImageTask extends AsyncTask<Void, Integer, Bitmap[]> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(PlayActivity.this);
            dialog.setTitle("Image Download");
            dialog.setMessage("Metadata ...");
            dialog.setCancelable(false);
            dialog.show();

        }

        @Override
        protected Bitmap[] doInBackground(Void... voids) {
            URL url;
            try {
                url = new URL("https://bitbucket.org/thefuntasty/test-android/raw/master/images.json");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader ((InputStream) conn.getContent());
                BufferedReader reader = new BufferedReader(in);
                StringBuilder data = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    data.append(line);
                }
                JSONObject obj = new JSONObject(new JSONTokener(data.toString()));
                JSONArray arr = obj.getJSONArray("images");
                final int total = arr.length();
                for(int i = 0; i < total; i++){

                    JSONObject entry = arr.getJSONObject(i);
                    String name = entry.getString("name");
                    String loc = entry.getString("url");

                    URL imageUrl = new URL(loc);
                    final Bitmap bmp = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());

                    final int current = i + 1;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            app.images.add(bmp);
                            adapter.notifyDataSetChanged();
                            dialog.setMessage("File "+current+" of " +total+" ...");
                        }
                    });

                }
            }
            catch (Exception e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PlayActivity.this);
                        builder.setTitle("Error");
                        builder.setMessage("Could not Load items from network");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Just quit
                                (PlayActivity.this).finish();
                            }
                        });
                        builder.show();
                    }
                });
            }
            finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.hide();

                    }
                });
            }
            return  null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater ii = getMenuInflater();
        ii.inflate(R.menu.play_menu,menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_history)
        {
            if (drawer.isDrawerOpen(Gravity.LEFT)){
                drawer.closeDrawer(Gravity.LEFT);
            }else {
                drawer.openDrawer(Gravity.LEFT);
            }
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linear = new LinearInterpolator();
        setContentView(R.layout.activity_play);

        app = (FruitMachineApplication)getApplication();

        adapter = new RVBitmapAdapter(app.images);

        button = (Button) findViewById(R.id.button);
        centerLayout =(SelectorLinearLayout) findViewById(R.id.center_square);

        drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerContents = (LinearLayout)findViewById(R.id.left_drawer);
        historyList = (ListView)findViewById(R.id.hist_list);
        layoutManager = new CustomLayoutManager(this);

        histAdapter = new ArrayAdapter<Integer>(getBaseContext(),R.layout.history_entry,app.historyData){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    LayoutInflater inflater = (LayoutInflater) PlayActivity.this
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.history_entry,parent,false);
                }
                TextView histStatus = (TextView)convertView.findViewById(R.id.history_status);
                TextView histDetail = (TextView)convertView.findViewById(R.id.history_detail);
                Integer entry = getItem(position);
                histStatus.setText(entry == 3 ? "Win": "Loss");
                histDetail.setText(entry == 2 ? "2 Matched "  : "No Match");
                return convertView;
            }
        };
        historyList.setAdapter(histAdapter);

        left = (RecyclerView) findViewById(R.id.scroll_left);
        center = (RecyclerView) findViewById(R.id.scroll_center);
        right = (RecyclerView) findViewById(R.id.scroll_right);

        left.setEnabled(false);
        center.setEnabled(false);
        right.setEnabled(false);

        left.setLayoutManager(new CustomLayoutManager(this));
        center.setLayoutManager(new CustomLayoutManager(this));
        right.setLayoutManager(new CustomLayoutManager(this));

        if(app.images.size() == 0) {
            // Load images from net asynchronously
            new ImageTask().execute();
        }

        left.setAdapter(adapter);
        center.setAdapter(adapter);
        right.setAdapter(adapter);

        // No user interaction with wheels
        left.setEnabled(false);
        center.setEnabled(false);
        right.setEnabled(false);

        left.addOnScrollListener(scrollListener);
        center.addOnScrollListener(scrollListener);
        right.addOnScrollListener(scrollListener);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (timer != null) {
                    //Cancel automatic rotations
                    timer.cancel();
                    timer = null;

                    leftScrolling = true;
                     centerScrolling = true;
                   rightScrolling = true;

                    //add randomized turns
                    int lcount = random.nextInt(5) + 10;
                    int ccount = random.nextInt(5) + 10;
                    int rcount = random.nextInt(5) + 10;

                    left.smoothScrollToPosition(leftIndex+lcount);
                    center.smoothScrollToPosition(centerIndex+ccount);
                    right.smoothScrollToPosition(rightIndex+rcount);

                } else if( !leftScrolling && !centerScrolling && !rightScrolling) {
                    SpinWheels();
                }
            }
        });
    }

    private void SpinWheels() {
        timer = new CountDownTimer(Long.MAX_VALUE, scrollPeriod) {
            public void onTick(long millisUntilFinished) {
                left.scrollBy(0,32+random.nextInt(5));
                center.scrollBy(0,32+random.nextInt(5));
                right.scrollBy(0,32+random.nextInt(5));
            }

            public void onFinish() {
            }
        };
        timer.start();

        button.setText("Stop");
    }

    private int leftIndex;
    private int centerIndex;
    private int rightIndex;

    private boolean leftScrolling;
    private boolean centerScrolling;
    private boolean rightScrolling;

    void recount() {
        int leftClamped = leftIndex % itemCount;
        int centerClamped = centerIndex % itemCount;
        int rightClamped = rightIndex % itemCount;

        if (leftClamped == centerClamped&&
                centerClamped == rightClamped) {
            button.setText("You WON!,  Play again ?");
            histAdapter.insert(3,0);
        } else {
            button.setText("You LOST!,  Play again ?");
            //If 3 are equal, we have won, if 2 are equal we display it
            int count = (leftClamped == centerClamped | centerClamped == rightClamped |leftClamped == rightClamped  ? 2 : 0);
            histAdapter.insert(count,0);
        }
    }

    RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if(newState  == RecyclerView.SCROLL_STATE_IDLE) {
                if (recyclerView == left) {
                    leftScrolling = false;
                }
                if (recyclerView == center) {
                    centerScrolling = false;
                }
                if (recyclerView == right) {
                    rightScrolling = false;
                }
            }
            if(!leftScrolling && !centerScrolling && !rightScrolling){
                recount();
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            RVBitmapAdapter.SlotViewHolder slot = (RVBitmapAdapter.SlotViewHolder)
                    recyclerView.getChildViewHolder(recyclerView.getLayoutManager().getChildAt(0));
            //Update indices current indices
            if (recyclerView == left) {
                leftIndex = slot.index;
            }
            if (recyclerView == center) {
                centerIndex = slot.index;
            }
            if (recyclerView == right) {
                rightIndex = slot.index;
            }
        }
    };
}
