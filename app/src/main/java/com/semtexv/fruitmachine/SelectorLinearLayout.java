package com.semtexv.fruitmachine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by Semtexzv on 4/13/2016.
 */
public class SelectorLinearLayout extends LinearLayout {

    private Drawable selector;
    public SelectorLinearLayout(Context context) {
        super(context);
        setWillNotDraw(false);
        selector = getContext().getResources().getDrawable(R.drawable.slot_selector);
    }

    public SelectorLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        selector = getContext().getResources().getDrawable(R.drawable.slot_selector);
    }

    public SelectorLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
        selector = getContext().getResources().getDrawable(R.drawable.slot_selector);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
            selector.setBounds(0, getHeight() / 3, getWidth(), (getHeight() / 3) * 2);
            selector.draw(canvas);
    }
}
