package com.semtexv.fruitmachine;

import android.app.Application;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.BitSet;

public class FruitMachineApplication extends Application {

    public ArrayList<Bitmap> images = new ArrayList<>();
    public ArrayList<Integer> historyData = new ArrayList<>();
}
